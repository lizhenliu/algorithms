package cqu.edu.chapter04;

import java.util.Random;

public class Test {

    public static void main(String[] args) {
//        int[] a = new int[]{10,11,12,16,18,23,29,33,48,54,68,77,84,98};
//        int key = 98;
//        BinarySearch binarySearch = new BinarySearch();
//        System.out.println("结果是：" + binarySearch.bs(a,key));

        BinarySearch binarySearch = new BinarySearch();
        int[] intArr = new int[300000000];
        for (int i = 0; i < intArr.length; ++i) {
            intArr[i] = i;
        }

        long startTime = System.currentTimeMillis();
        System.out.println("结果是：" + binarySearch.bs(intArr,0));
        long endTime = System.currentTimeMillis();
        System.out.println("系统运行时间：" + (endTime - startTime));

    }
}
