package cqu.edu.chapter04;

import cqu.edu.chapter03.rightControl;

import java.util.Arrays;

public class BinarySearch {



    public int bs(int[] arr, int key){
        int leftIndex = 0;
        int rightIndex = arr.length;
        while(leftIndex < rightIndex){
            int middleIndex = leftIndex + (rightIndex - leftIndex)/2; // middleIndex = 0 的情况没有发生
            if (key < arr[middleIndex]){
                rightIndex = middleIndex - 1;
            }else if (arr[middleIndex] < key){
                leftIndex = middleIndex + 1;
            }else {
                return middleIndex;
            }
        }
        return -2;//返回-1代表查找失败
    }


}
